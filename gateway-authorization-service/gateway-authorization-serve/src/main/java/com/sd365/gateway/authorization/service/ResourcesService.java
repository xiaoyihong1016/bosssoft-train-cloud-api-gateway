package com.sd365.gateway.authorization.service;

import com.sd365.gateway.authorization.entity.Resource;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:50
 * @Version 1.0
 * @description
 */
@CacheConfig(cacheNames = "ResourcesService")
public interface ResourcesService {

    @CachePut(key = "#p0.name")
    Boolean addResource(Resource resource);

    @CacheEvict(key = "#p0")
    Boolean removeResource(Long id);

    List<Resource> searchResource(List<Long> ids);


    Boolean batchAddResource(List<Resource> resources);


    Boolean BatchRemoveResource(List<Long> ids);

    @Cacheable(key = "#p0")
    Resource queryById(Long id);
}
