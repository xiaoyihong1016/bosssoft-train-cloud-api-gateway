package com.sd365.gateway.authorization.dao.mapper;

import com.sd365.gateway.authorization.entity.RoleResource;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface RoleResourceMapper extends Mapper<RoleResource> {

}